package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSum(t *testing.T) {
	tests := []struct {
		name        string
		n1          int
		n2          int
		expectedAns int
	}{
		{
			name:        "test 1",
			n1:          100,
			n2:          300,
			expectedAns: 400,
		},
		{
			name:        "test 2",
			n1:          100,
			n2:          -300,
			expectedAns: -200,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ans := sum(test.n1, test.n2)
			if ans != test.expectedAns {
				t.Logf("expect ans to be %v got %v", test.expectedAns, ans)
				t.FailNow()
			}
		})
	}
}

func TestSumHandler(t *testing.T) {
	data := map[string]int{
		"a": 1,
		"b": 2,
	}

	var body bytes.Buffer
	if err := json.NewEncoder(&body).Encode(data); err != nil {
		t.Logf("expect no error got %v", err)
		t.FailNow()
	}

	req, err := http.NewRequest(http.MethodPost, "/sum", &body)
	if err != nil {
		t.Logf("expect no error got %v", err)
		t.FailNow()
	}

	w := httptest.NewRecorder()
	sumHandler(w, req)

	resp := w.Result()
	if resp.StatusCode != http.StatusOK {
		t.Logf("expect status code to be %v got %v", http.StatusOK, resp.StatusCode)
		t.FailNow()
	}
}
