package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Numbers struct {
	Number1 int `json:"a"`
	Number2 int `json:"b"`
}

type Answer struct {
	Result       int    `json:"result"`
	ErrorMessage string `json:"-"`
}

func sum(n1, n2 int) int {
	return n1 + n2
}

func sumHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	ans := Answer{}
	numbers := Numbers{}

	var resp bytes.Buffer
	encoder := json.NewEncoder(&resp)

	if err := json.Unmarshal(b, &numbers); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		ans.ErrorMessage = fmt.Sprintf("error while unmarshaling err: %v", err)
		encoder.Encode(ans)
		w.Write(resp.Bytes())
		return
	}

	ans.Result = sum(numbers.Number1, numbers.Number2)

	if err := encoder.Encode(ans); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		errMsg := fmt.Sprintf("error while encoding json err: %v", err)
		w.Write([]byte(errMsg))
		return
	}

	w.Write(resp.Bytes())
}

func main() {
	http.HandleFunc("/sum", sumHandler)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		w.Write([]byte("hello world"))
	})

	log.Println("Listening at port :8080")
	http.ListenAndServe(":8080", nil)
}
